﻿using Common;
using EntityModel.EF;
using EntityModel.Services;
using System;
using System.Linq;

namespace EntityModel.BLL
{
    public static class clsLogin
    {
        public static Define.fLogin CheckLogin(this IUnitOfWork instance, string username, string password)
        {
            xAccount account = instance.GetRepository<xAccount>().GetItems().FirstOrDefault(x => x.Username.ToLower().Equals(username.ToLower()) && x.Password.ToLower().Equals(password.ToLower()));

            if (account != null)
            {
                if (account.IsEnable)
                    return Define.fLogin.Success;
                else
                    return Define.fLogin.Disable;
            }
            else
            {
                return Define.fLogin.NotFound;
            }
        }

        public static bool CheckExist(this IUnitOfWork instance, string username)
        {
            xAccount account = instance.GetRepository<xAccount>().GetItems().FirstOrDefault(x => x.Username.ToLower().Equals(username.ToLower()));

            if (account != null)
                return true;
            return false;
        }
    }
}