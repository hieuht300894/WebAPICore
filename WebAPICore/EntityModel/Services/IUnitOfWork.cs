﻿using System.Threading.Tasks;

namespace EntityModel.Services
{
    public interface IUnitOfWork
    {
        void BeginTransaction();
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void CommitTransaction();
        void RollbackTransaction();
        IRepository<T> GetRepository<T>() where T : class, new();
    }
}