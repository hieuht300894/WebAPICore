using EntityModel.General;
using EntityModel.Interface;

namespace EntityModel.EF
{
    public partial class xAccount : Master, IPersonnel, IPermissionCategory
    {
        public string IPAddress { get; set; } = string.Empty;
        public int IDPersonnel { get; set; } = 0;
        public string PersonnelCode { get; set; } = string.Empty;
        public string PersonnelName { get; set; } = string.Empty;
        public int IDPermissionCategory { get; set; } = 0;
        public string PermissionCategoryCode { get; set; } = string.Empty;
        public string PermissionCategoryName { get; set; } = string.Empty;
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public bool IsEnable { get; set; } = false;
    }
}
