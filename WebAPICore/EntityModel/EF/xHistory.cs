﻿using EntityModel.General;

namespace EntityModel.EF
{
    public class xHistory : Master
    {
        public string Action { get; set; } = string.Empty;
        public string Table { get; set; } = string.Empty;
        public string OldRecord { get; set; } = string.Empty;
        public string NewRecord { get; set; } = string.Empty;
    }
}
