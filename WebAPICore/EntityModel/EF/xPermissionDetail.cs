using EntityModel.General;
using EntityModel.Interface;

namespace EntityModel.EF
{
    public class xPermissionDetail : Master, IPermissionCategory, IPermission
    {
        public int IDPermissionCategory { get; set; } = 0;
        public string PermissionCategoryCode { get ; set ; } = string.Empty;
        public string PermissionCategoryName { get ; set ; } = string.Empty;
        public int IDPermission { get; set; } = 0;
        public string Controller { get ; set ; } = string.Empty;
        public string Action { get ; set ; } = string.Empty;
        public string Method { get ; set ; } = string.Empty;
        public string Template { get ; set ; } = string.Empty;
        public string Path { get ; set ; } = string.Empty;
    }
}
