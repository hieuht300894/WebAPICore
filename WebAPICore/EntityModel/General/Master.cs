﻿using EntityModel.Interface;
using System;

namespace EntityModel.General
{
    public class Master : IEF, IMaster
    {
        public int KeyID { get; set; } = AutoGenerateID.KeyID;
        public string Code { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public int CreatedBy { get; set; } = 0;
        public DateTime? ModifiedDate { get; set; } = null;
        public int? ModifiedBy { get; set; } = null;
        public string Note { get; set; }= string.Empty;
        public int Status { get; set; } = 0;
        public bool IsDefault { get; set; } = false;
    }
}
