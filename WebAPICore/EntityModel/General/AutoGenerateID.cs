﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntityModel.General
{
    public class AutoGenerateID
    {
        private static int _keyID = 0;
        public static int KeyID { get { return _keyID--; } }
    }
}