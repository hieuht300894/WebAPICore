﻿using Common;
using EntityModel.OtherEF;
using EntityModel.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    public class BaseController<T> : ControllerBase where T : class, new()
    {
        protected IUnitOfWork Instance;

        public BaseController(IUnitOfWork unitOfWork)
        {
            Instance = unitOfWork;
        }

        [HttpGet("GetCode/{prefix}")]
        public virtual async Task<IActionResult> GetCode(string prefix)
        {
            String bRe = prefix.ToUpper() + DateTime.Now.ToString("yyyyMMdd");
            DateTime time = DateTime.Now;
            try
            {
                IEnumerable<T> lstTemp = await Instance.GetRepository<T>().GetItemsAsync();
                T Item = lstTemp.OrderByDescending<T, Int32>("KeyID").FirstOrDefault();
                if (Item == null)
                {
                    bRe += "0001";
                }
                else
                {
                    String Code = Item.GetObjectValueByName<String>("Code");
                    if (Code.StartsWith(bRe))
                    {
                        Int32 number = Int32.Parse(Code.Replace(bRe, String.Empty));
                        ++number;
                        bRe = String.Format("{0}{1:0000}", bRe, number);
                    }
                    else
                        bRe += "0001";
                }
                return Ok(bRe);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpGet("GetItemsPage/{pageIndex?}")]
        public virtual async Task<IActionResult> GetItemsPage(int? pageIndex)
        {
            if (!pageIndex.HasValue || pageIndex == 0 || pageIndex == 1)
                return Ok(await Instance.GetRepository<T>().GetItemsAsync());

            return Ok(await Instance.GetRepository<T>().GetItemsAsync());
        }

        [HttpGet("GetItem/{id?}")]
        public virtual async Task<IActionResult> GetItem(int? id)
        {
            try
            {
                if (id == null)
                    return BadRequest();

                T item = await Instance.GetRepository<T>().FindItemAsync(id.HasValue ? id.Value : 0);
                if (item == null)
                    return NotFound();

                return Ok(item);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpGet("GetItems")]
        public virtual async Task<IActionResult> GetItems()
        {
            try
            {
                IEnumerable<T> lstTemp = await Instance.GetRepository<T>().GetItemsAsync();
                //IList<T> lstResult = lstTemp.OrderBy<T, String>("Ten").ToList();
                List<T> lstResult = lstTemp.ToList();
                return Ok(lstResult);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpPost("AddItem")]
        public virtual async Task<IActionResult> AddItem([FromBody] DataRequest<T> item)
        {
            try
            {
                if (item == null)
                    return BadRequest();

                Instance.BeginTransaction();
                await Instance.GetRepository<T>().AddItemAsync(item.NewData);
                await Instance.SaveChangesAsync();
                Instance.CommitTransaction();
                return Ok(item.NewData);
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpPost("AddItems")]
        public virtual async Task<IActionResult> AddItems([FromBody] DataRequest<T>[] items)
        {
            try
            {
                if (items == null || items.Length == 0)
                    return BadRequest();

                Instance.BeginTransaction();
                await Instance.GetRepository<T>().AddItemsAsync(items.Select(x => x.NewData).ToArray());
                await Instance.SaveChangesAsync();
                Instance.CommitTransaction();
                return Ok(items.Select(x => x.NewData).ToArray());
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpPut("UpdateItem")]
        public virtual async Task<IActionResult> UpdateItem([FromBody] DataRequest<T> item)
        {
            try
            {
                if (item == null)
                    return BadRequest();

                Instance.BeginTransaction();
                Instance.GetRepository<T>().UpdateItem(item.NewData);
                await Instance.SaveChangesAsync();
                Instance.CommitTransaction();
                return Ok(item.NewData);
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpPut("UpdateItems")]
        public virtual async Task<IActionResult> UpdateItems([FromBody] DataRequest<T>[] items)
        {
            try
            {
                if (items == null || items.Length == 0)
                    return BadRequest();

                Instance.BeginTransaction();
                Instance.GetRepository<T>().UpdateItems(items.Select(x => x.NewData).ToArray());
                await Instance.SaveChangesAsync();
                Instance.CommitTransaction();
                return Ok(items.Select(x => x.NewData).ToArray());
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("DeleteItem")]
        public virtual async Task<IActionResult> DeleteItem([FromBody] DataRequest<T> item)
        {
            try
            {
                if (item == null)
                    return BadRequest();

                Instance.BeginTransaction();
                Instance.GetRepository<T>().RemoveItem(item.OldData);
                await Instance.SaveChangesAsync();
                Instance.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("DeleteItems")]
        public virtual async Task<IActionResult> DeleteItems([FromBody] DataRequest<T>[] items)
        {
            try
            {
                if (items == null || items.Length == 0)
                    return BadRequest();

                Instance.BeginTransaction();
                Instance.GetRepository<T>().RemoveItems(items.Select(x => x.OldData).ToArray());
                await Instance.SaveChangesAsync();
                Instance.CommitTransaction();
                return Ok();
            }
            catch (Exception ex)
            {
                Instance.RollbackTransaction();
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }

        [HttpGet("Test")]
        public virtual IActionResult Test()
        {
            try
            {
                DataRequest<T> item = new DataRequest<T>();
                item.OldData = new T();
                item.NewData = new T();
                return Ok(item);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(Exception), ex.Message);
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return BadRequest(ModelState);
            }
        }
    }
}