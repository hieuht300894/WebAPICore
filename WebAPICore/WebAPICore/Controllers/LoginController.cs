﻿using Common;
using EntityModel.BLL;
using EntityModel.EF;
using EntityModel.OtherEF;
using EntityModel.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebAPICore.Controllers
{
    public class LoginController : BaseController<xAccount>
    {
        public LoginController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpPost]
        public IActionResult SignIn(LoginRequest login)
        {
            bool IsValid = true;

            if (login == null)
            {
                ModelState.AddModelError("", "Đăng nhập không thành công.");
                IsValid = false;
            }
            if (login.Username.IsEmpty())
            {
                ModelState.AddModelError(nameof(login.Username), "Vui lòng nhập tài khoản.");
                IsValid = false;
            }
            if (login.Password.IsEmpty())
            {
                ModelState.AddModelError(nameof(login.Password), "Vui lòng nhập mật khẩu.");
                IsValid = false;
            }

            if (IsValid)
            {
                Define.fLogin res = Instance.CheckLogin(login.Username, login.Password);
                switch (res)
                {
                    case Define.fLogin.NotFound:
                        ModelState.AddModelError("", "Tài khoản không tồn tại.");
                        goto default;
                    case Define.fLogin.Disable:
                        ModelState.AddModelError("", "Tài khoản đã bị khóa.");
                        goto default;
                    case Define.fLogin.Success:
                        return Ok();
                    default:
                        return BadRequest(ModelState);
                }
            }
            return BadRequest(ModelState);
        }
    }
}