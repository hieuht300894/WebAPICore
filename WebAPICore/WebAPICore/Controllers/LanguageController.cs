﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityModel.EF;
using EntityModel.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPICore.Controllers
{
    public class LanguageController : BaseController<xLanguage>
    {
        public LanguageController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
